﻿namespace DesignPaterns.AbstractFactory.Interface
{
    interface IHotDrink
    {
        string Consume();
    }
}
